# Copyright 2020 The Kubeflow Authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import argparse
import datetime
from distutils.util import strtobool
import json
import os
import logging
import time
import config
import sys

from kubernetes.client import V1ObjectMeta

from kubeflow.katib import KatibClient
from kubeflow.katib import ApiClient
from kubeflow.katib import V1beta1Experiment
from kubeflow.katib import V1beta1ExperimentSpec
from kubeflow.katib import V1beta1AlgorithmSpec
from kubeflow.katib import V1beta1EarlyStoppingSpec
from kubeflow.katib import V1beta1EarlyStoppingSetting
from kubeflow.katib import V1beta1ObjectiveSpec
from kubeflow.katib import V1beta1MetricStrategy
from kubeflow.katib import V1beta1ParameterSpec
from kubeflow.katib import V1beta1FeasibleSpace
from kubeflow.katib import V1beta1MetricsCollectorSpec
from kubeflow.katib import V1beta1CollectorSpec
from kubeflow.katib import V1beta1SourceSpec
from kubeflow.katib import V1beta1FilterSpec
from kubeflow.katib import V1beta1FileSystemPath
from kubeflow.katib import V1beta1TrialTemplate
from kubeflow.katib import V1beta1TrialParameterSpec

logger = logging.getLogger()
logging.basicConfig(level=logging.INFO)

FINISH_CONDITIONS = ["Succeeded", "Failed"]


class JSONObject(object):
    """ This class is needed to deserialize input JSON.
    Katib API client expects JSON under .data attribute.
    """

    def __init__(self, json):
        self.data = json

def parse_args(args):
    parser = argparse.ArgumentParser(description='Parse variables to katib pipeline')
    parser.add_argument('--experiment-name',
                        default=config.EXPERIMENT_NAME,
                        type=str,
                        help='Experiment name')
    parser.add_argument('--experiment-namespace',
                        default=config.EXPERIMENT_NAMESPACE,
                        type=str,
                        help='Experiment namespace')
    parser.add_argument("--dataset-dir",
                        default=config.DATASET_DIR,
                        type=str,
                        help="Dataset Directory")
    parser.add_argument("--log-dir",
                        default=config.LOG_DIR,
                        type=str,
                        help="Path to save summary writer logs.")
    parser.add_argument('--max-trial-count',
                        default=config.MAX_TRIAL_COUNT,
                        type=int,
                        help='Number of Max trial count')
    parser.add_argument('--max-failed-trial-count',
                        default=config.MAX_FAILED_TRIAL_COUNT,
                        type=int,
                        help='Number of Max failed trial allowed')
    parser.add_argument('--parallel-trial-count',
                        default=config.PARALLEL_TRIAL_COUNT,
                        type=int,
                        help='Number of parallel katib job want to run')
    parser.add_argument('--loss-goal',
                        default=config.LOSS_GOAL,
                        type=float,
                        help='Goal of loss value to achieve during katib')
    parser.add_argument('--epochs',
                        default=config.EPOCHS,
                        type=int,
                        metavar="N",
                        help='number of epochs to train (default: 100)')
    parser.add_argument('--min-learning-rate',
                        default=config.MIN_LEARNING_RATE,
                        type=float,
                        help='Min Learning Rate value')
    parser.add_argument('--max-learning-rate',
                        default=config.MAX_LEARNING_RATE,
                        type=float,
                        help='Max Learning Rate value')
    parser.add_argument('--min-momentum',
                        default=config.MIN_MOMENTUM,
                        type=float,
                        help='Min Momentum value')
    parser.add_argument('--max-momentum',
                        default=config.MAX_MOMENTUM,
                        type=float,
                        help='Max Momentum value')
    parser.add_argument('--experiment-timeout-minutes',
                        default=config.EXPERIMENT_TIMEOUT_MINUTES,
                        type=int,
                        help='Time in minutes to wait for the Experiment to complete')
    parser.add_argument('--delete-after-done',
                        default=config.DELETE_AFTER_DONE,
                        type=strtobool,
                        help='Whether to delete the Experiment after it is finished')
    parser.add_argument('--container-image',
                        default=config.CONTAINER_IMAGE,
                        type=str,
                        help='Which Docker Container Image is used for katib experiment')
    parser.add_argument('--output-file',
                        default=config.OUTPUT_DIR + '/output.txt',
                        type=str,
                        help='The file which stores the best hyperparameters of the Experiment')
    return parser.parse_args(args)

def wait_experiment_finish(katib_client, experiment, timeout):
    polling_interval = datetime.timedelta(seconds=30)
    end_time = datetime.datetime.now() + datetime.timedelta(minutes=timeout)
    experiment_name = experiment.metadata.name
    experiment_namespace = experiment.metadata.namespace
    while True:
        current_status = None
        try:
            current_status = katib_client.get_experiment_status(name=experiment_name, namespace=experiment_namespace)
        except Exception as e:
            logger.info("Unable to get current status for the Experiment: {} in namespace: {}. Exception: {}".format(
                experiment_name, experiment_namespace, e))
        # If Experiment has reached complete condition, exit the loop.
        if current_status in FINISH_CONDITIONS:
            logger.info("Experiment: {} in namespace: {} has reached the end condition: {}".format(
                experiment_name, experiment_namespace, current_status))
            return
        # Print the current condition.
        logger.info("Current condition for Experiment: {} in namespace: {} is: {}".format(
            experiment_name, experiment_namespace, current_status))
        # If timeout has been reached, rise an exception.
        if datetime.datetime.now() > end_time:
            raise Exception("Timeout waiting for Experiment: {} in namespace: {} "
                            "to reach one of these conditions: {}".format(
                                experiment_name, experiment_namespace, FINISH_CONDITIONS))
        # Sleep for poll interval.
        time.sleep(polling_interval.seconds)

def create_experiment():
    args = parse_args(sys.argv[1:])
    experiment_name = args.experiment_name
    experiment_namespace = args.experiment_namespace
    experiment_spec = {
        "algorithm":{
            "algorithmName":"random"
        },
        "maxFailedTrialCount":str(args.max_failed_trial_count),
        "maxTrialCount":str(args.max_trial_count),
        "objective":{
            "goal":str(args.loss_goal),
            "objectiveMetricName":"loss",
            "type":"minimize"
        },
        "parallelTrialCount":str(args.parallel_trial_count),
        "parameters":[
            {
                "feasibleSpace":{
                    "min":str(args.min_learning_rate),
                    "max":str(args.max_learning_rate)
                },
                "name":"lr",
                "parameterType":"double"
            },
            {
                "feasibleSpace":{
                    "min":str(args.min_momentum),
                    "max":str(args.max_momentum)
                },
                "name":"momentum",
                "parameterType":"double"
            }
        ],
        "trialTemplate":{
            "primaryContainerName":"pytorch",
            "retain":"true",
            "trialParameters":[
                {
                    "description":"Learning-Rate",
                    "name":"learningRate",
                    "reference":"lr"
                },
                {
                    "description":"Momentum",
                    "name":"momentum",
                    "reference":"momentum"
                }
            ],
            "trialSpec":{
                "apiVersion":"kubeflow.org/v1",
                "kind":"PyTorchJob",
                "spec":{
                    "pytorchReplicaSpecs":{
                        "Master":{
                            "replicas":1,
                            "restartPolicy":"ExitCode",
                            "template":{
                                "metadata":{
                                    "annotations":{
                                    "sidecar.istio.io/inject":"false"
                                    }
                                },
                                "spec":{
                                    "containers":[
                                        {
                                            "name":"pytorch",
                                            "image":args.container_image,
                                            "command":[
                                                "python3",
                                                "src/fashion_mnist.py",
                                                "--dataset-path=" + args.dataset_dir,
                                                "--log-dir=" + args.log_dir,
                                                "--epochs=" + str(args.epochs),
                                                "--lr=${trialParameters.learningRate}",
                                                "--momentum=${trialParameters.momentum}"
                                            ],
                                            "resources":{
                                                "limits":{
                                                    "nvidia.com/gpu":"1"
                                                }
                                            },
                                            "volumeMounts":[
                                                {
                                                    "mountPath":config.STORAGE_VOLUME_MOUNT_PATH,
                                                    "name":config.STORAGE_PV_NAME
                                                },
                                                {
                                                    "mountPath":config.MINIO_VOLUME_MOUNT_PATH,
                                                    "name":config.MINIO_PV_NAME
                                                }
                                            ]
                                        }
                                    ],
                                    "volumes":[
                                        {
                                            "name":"STORAGE-pv-volume",
                                            "persistentVolumeClaim":{
                                                "claimName":config.STORAGE_PVC_NAME
                                            }
                                        },
                                        {
                                            "name":"minio-rwx-pv-admin",
                                            "persistentVolumeClaim":{
                                                "claimName":config.MINIO_PVC_NAME
                                            }
                                        }
                                    ]
                                }
                            }
                        }
                    }
                }
            }
        }
    }    

    # Create JSON dump from experiment spec
    experiment_spec = json.dumps(experiment_spec)
    # Create JSON object from experiment spec
    experiment_spec = JSONObject(experiment_spec)
    # Deserialize JSON to ExperimentSpec
    experiment_spec = ApiClient().deserialize(experiment_spec, "V1beta1ExperimentSpec")

    # Create Experiment object.
    experiment = V1beta1Experiment(
        api_version="kubeflow.org/v1beta1",
        kind="Experiment",
        metadata=V1ObjectMeta(
            name=experiment_name,
            namespace=experiment_namespace
        ),
        spec=experiment_spec
    )

    # Print experiment
    print(f'experiment:{experiment}')
    # Create Katib client.
    print(f'b1')
    # maybe self certificate issue
    # https://www.kubeflow.org/docs/components/pipelines/sdk/connect-api/#multi-user-mode
    katib_client = KatibClient()
    print(f'b2')
    # Create Experiment in Kubernetes cluster.
    output = katib_client.create_experiment(experiment, namespace=experiment_namespace)
    print(f'b3')

    # Wait until Experiment is created.
    end_time = datetime.datetime.now() + datetime.timedelta(minutes=args.experiment_timeout_minutes)
    while True:
        current_status = None
        # Try to get Experiment status.
        try:
            current_status = katib_client.get_experiment_status(name=experiment_name, namespace=experiment_namespace)
        except Exception:
            logger.info("Waiting until Experiment is created...")
        # If current status is set, exit the loop.
        if current_status is not None:
            break
        # If timeout has been reached, rise an exception.
        if datetime.datetime.now() > end_time:
            raise Exception("Timout waiting for Experiment: {} in namespace: {} to be created".format(
                experiment_name, experiment_namespace))
        time.sleep(1)

    logger.info("Experiment is created")

    # Wait for Experiment finish.
    wait_experiment_finish(katib_client, experiment, args.experiment_timeout_minutes)

    # Check if Experiment is successful.
    if katib_client.is_experiment_succeeded(name=experiment_name, namespace=experiment_namespace):
        logger.info("Experiment: {} in namespace: {} is successful".format(
            experiment_name, experiment_namespace))

        optimal_hp = katib_client.get_optimal_hyperparameters(
            name=experiment_name, namespace=experiment_namespace)
        logger.info(f'Optimal hyperparameters:\n{optimal_hp}')
        # Check output_file path
        logger.info(f'args.output_file:\n{args.output_file}')
        # Create dir if it doesn't exist.
        if not os.path.exists(os.path.dirname(args.output_file)):
            os.makedirs(os.path.dirname(args.output_file))
        # Save HyperParameters to the file.
        with open(args.output_file, 'w') as f:
            f.write(json.dumps(optimal_hp))
    else:
        logger.info("Experiment: {} in namespace: {} is failed".format(
            experiment_name, experiment_namespace))
        # Print Experiment if it is failed.
        experiment = katib_client.get_experiment(name=experiment_name, namespace=experiment_namespace)
        logger.info(experiment)

    # Delete Experiment if it is needed.
    if args.delete_after_done:
        katib_client.delete_experiment(name=experiment_name, namespace=experiment_namespace)
        logger.info("Experiment: {} in namespace: {} has been deleted".format(
            experiment_name, experiment_namespace))

if __name__ == "__main__":
    create_experiment()
