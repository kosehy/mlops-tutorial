import os

EXPERIMENT_NAME = "Test"
DATASET_DIR = "./data"
OUTPUT_DIR = "/app/minio/output/"
LOG_DIR = os.path.join(OUTPUT_DIR, "logs", EXPERIMENT_NAME)

# Katib setting
EXPERIMENT_NAMESPACE = "kubeflow-user-example-com"
MAX_TRIAL_COUNT = 1
MAX_FAILED_TRIAL_COUNT = 1
PARALLEL_TRIAL_COUNT = 1
LOSS_GOAL = 0.001
EPOCHS = 1
MIN_LEARNING_RATE = 1e-2
MAX_LEARNING_RATE = 5e-2
MIN_MOMENTUM = 0.5
MAX_MOMENTUM = 0.9

EXPERIMENT_TIMEOUT_MINUTES = 60*24*20
DELETE_AFTER_DONE = False
CONTAINER_IMAGE = "kosehy/fashion_mnist:latest"

STORAGE_VOLUME_MOUNT_PATH = "/app/storage"
MINIO_VOLUME_MOUNT_PATH = "/app/minio"
STORAGE_PV_NAME = "STORAGE-pv-volume"
MINIO_PV_NAME = "minio-rwx-pv-admin"
STORAGE_PVC_NAME = "STORAGE-pvc-volume"
MINIO_PVC_NAME = "minio-rwx-pvc-admin"

# Pipeline setting
PIPELINE_NAME = "fashion_mnist_katib_pipeline"
DESCRIPTION = "Test fashion_mnist katib pipeline"

S3_ENDPOINT = "minio-service.kubeflow.svc.cluster.local:9000"
AWS_ENDPOINT_URL = "http://" + S3_ENDPOINT
AWS_ACCESS_KEY_ID = "minio"
AWS_SECRET_ACCESS_KEY = "minio123"
AWD_REGION = "us-east-1"
MINIO_ADDR = 'http://192.168.35.101:9000/'

PROJECT_NAME = 'fashion_mnist-katib'
PIPELINE_NAME = PROJECT_NAME + '-pipeline'

EXPERIMENT_VERSION = 'v1'
KATIB_EXPERIMENT_NAME = PIPELINE_NAME + '-' + EXPERIMENT_VERSION

RANDOM_HOSTNAME = '/' + '$(cat /etc/hostname)'
MODEL_DIR = '/workspace/minio/tensorboard/project/' + PROJECT_NAME + '/model/' + KATIB_EXPERIMENT_NAME + '/'
MODEL_DIR_WITH_HOST = MODEL_DIR + RANDOM_HOSTNAME
LOG_DIR = '/project/' + PROJECT_NAME + '/model/' + KATIB_EXPERIMENT_NAME + '/'
MINIO_PATH = 'minio/tensorboard/project/' + PROJECT_NAME + '/model/' + KATIB_EXPERIMENT_NAME + '/'

# Kubeflow auth info
USERNAME = "user@example.com"
PASSWORD = "12341234"
NAMESPACE = 'kubeflow-user-example-com'
HOST = 'https://192.168.35.102/'
